# Contributing

The FVLib code is not currently an open-source project, however, code releases for the general public are planned in a near future. In the meantime, anyone willing to contribute to the project is welcome and can request access to the code under a collaboration agreement, sending an e-mail to rcosta(at)dep.uminho.pt.

## Contributors

The FVLib project had the contribution of several people:

- Gaspar J. Machado (University of Minho)
- Teresa Malheiro (University of Minho)
- Hélder Barbosa (University of Minho)

