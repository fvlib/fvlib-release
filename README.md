# FVLib-Release

The Great Finite Volume Library release version.

## 1. Description

The FVLib code is a library of advanced computational algorithms and numerical methods to solve partial differential equations (PDEs) within the finite volume philosophy. The project aims to bring high-accurate, high-performance, and high-efficient simulations of a wide range of physics and mechanics problems in relevant industrial, environmental, and biomedical applications. In that regard, the development of the FVLib code is based on the following principles:

- *Modern object-oriented Fortran (2003/2008 standards)* - for better code reuse and organization
- *High-scalability with multi-threading execution* - to take advantage of modern HPC systems
- *Unstructured meshes with general element shapes* - for complex geometries in real applications
- *Very high-order of convergence both in space and time* - for highly-accurate and reliable solutions

Currently, the following problems can be solved in the FVLib code:

- Convection-diffusion problems for heat and species transfer
- Conjugate heat transfer with solid/solid and solid/fluid interfaces
- Incompressible isothermal fluid flows with the Euler/Stokes/Navier-Stokes formulation
- Incompressible non-isothermal fluid flows with the Euler/Stokes/Navier-Stokes formulation
- Incompressible non-Newtonian fluid flows with the Stokes/Navier-Stokes formulation

## 2. Download

The FVLib code package must be downloaded to any directory of your choice (avoid directories with whitespaces in their name or in their parent directories). Usually, `$HOME` is recommended to install the FVLib code only for the current user, whereas `/opt` or `/share` is preferable to make the installation accessible for all the users in the system. Several options exist to download and uncompress the FVLib code:

- Clone using Git (recommended):
```
git clone https://gitlab.com/fvlib/fvlib-release.git fvlib-release
```

- Download using curl in tar.gz format:
```
curl https://gitlab.com/fvlib/fvlib-release/-/archive/main/fvlib-release-main.tar.gz --output fvlib-release-main
tar -xzvf fvlib-release-main.tar.gz && mv fvlib-release-main fvlib-release
```

- Download using curl in zip format:
```
curl https://gitlab.com/fvlib/fvlib-release/-/archive/main/fvlib-release-main.zip --output fvlib-release-main
unzip fvlib-release-main.zip && mv fvlib-release-main fvlib-release
```

## 3. Requirements

The FVLib code can be installed on any Linux and Unix-based operating systems. It requires a modern Fortran compiler for the 2003/2008 standards and, additionally, OpenMP-3 or above for multi-threading execution. The following list of compilers are tested and supported:

- GNU Compiler Collection (GCC), version 4.9 or above.
- Fujitsu Technical Computing Suite (TCSDS), version 1.2.26 or above.
- Arm Compiler for Linux (ACFL), version 20.3 or above.

For a parallel installation (recommended), a build automation tool is also required. The following list of make utilities are tested and supported:

- GNU Make (Make), version 3.8 or above.

> **NOTE**: The choice of the compiler version strongly determines the installation time, as well as the processor of the computer where the FVLib code is being installed. Newer versions require significantly more time to compile, but more aggressive optimizations are usually performed. Choose the compiler version wisely: for developing and testing on your personal computer, GCC version 5.0 or 6.0 is recommended for short build times; for intensive testing in high-performance computing systems, GCC version 7.0 or above is preferable.

## 4. Installation mode

The default configuration installs the FVLib code in production mode, meaning that the compiled library and applications are optimized for performance rather than for debugging. On the other side, for developing and testing, the debugging mode is recommended to enable verification routines and specific compiler debugging options.

Only either production or debugging mode should be used for the current installation. In order to have both modes available on your computer, create two installations in separate folders. For instance, `fvlib-release/` for the production mode and `fvlib-release-dbg/` for the debugging mode. Change the directoty name accordingly after download.

## 5. Quick installation (auto-configuration)

An auto-configuration utility is provided to install the FVLib code with the default configuration on an x86_64 or AArch64 architectures with the GCC compiler and the GNU make utility. For different configurations, see the detailed instructions. Before proceeding, make sure the `gfortran` and `make` commands are available on your computer.

### Production mode

Open a bash shell on the FVLib directory and execute the auto-configuration utility:
```
etc/autoconfig --install
```

### Debugging mode

Open a bash shell on the FVLib directory and execute the auto-configuration utility:
```
etc/autoconfig --debugging --install
```

## 6. Custom installation (detailed instructions)

Open a bash shell on the FVLib directory and follow the instructions below.

### Step 1

Make sure the compiler and the other required utilities are correctly configured on your computer and check whether the installed version is compatible, as provided in the requirements section:

- GCC compiler:
```
gfortran --version
```

- Fujitsu compiler:
```
frt --version
```

- Arm compiler:
```
armflang --version
```

- Make:
```
make --version
```

> **NOTE**: In Ubuntu and other APT-based Linux distributions, gfortran and make can be easily installed with `sudo apt-get install gfortran make`. For other distributions or to install from source, follow the instructions provided [here](https://fortran-lang.org/learn/os_setup/install_gfortran).

### Step 2

The default configuration of the FVLib code is to compile with the GCC compiler (gfortran) on an x86_64 architecture. For other combinations of compiler-architecture, several configurations files are provided inside the `etc/` folder. Rename the most appropriate configuration file to `compiler.sh`:

- GNU compiler in AArch64 architecture:
```
cp etc/gfortran-aarch64.sh etc/compiler.sh
```

- Fujitsu compiler in AArch64 architecture:
```
cp etc/fuji-aarch64.sh etc/compiler.sh
```

- Arm compiler in AArch64 architecture:
```
cp etc/arm-aarch64.sh etc/compiler.sh
```

### Step 3

The FVLib code provides a library management utility to compile, update, clean and create dependencies of the library and applications. Source the global configurations file:
```
source etc/bashrc.sh
```

Make sure the global configurations file is correctly sourced and the library management utility is available on your computer:
```
fvl-make --version
```

The installation of the FVLib code can be performed in serial or parallel. Since the installation might take several hours to finish depending on the compiler and the processor, the second option is usually preferable.

#### Production mode

- For a serial installation of the library and applications run:
```
fvl-make --all
```

- For a parallel installation of the library and applications, generate the Makefile with targets and dependencies for the make utility:
```
fvl-make --make
```

A file is created inside the `build/` folder, from where the make utility can be run:
```
cd build
make all -s -j
```

#### Debugging mode

- For a serial installation of the library and applications run:
```
fvl-make --debugging --all
```

- For a parallel installation of the library and applications, generate the Makefile with targets and dependencies for the make utility:
```
fvl-make --debugging --make
```

A file is created inside the `build/` folder, from where the make utility can be run:
```
cd build
make all -s -j
```

> **NOTE**: The compilation of the FVLib code and applications can take several hours to finish depending on the compiler and the processor. In the serial installation, a percentage is shown on the terminal to indicate the progress. In case the installation needs to be stopped and later resumed, simply run the last command again and the installation will proceed from the previous state.

> **NOTE**: For finer control, the build stages for the library and applications can be selected or deselected individually. Replace the `all` option with the `library` option to install the library, then with the `tools` option to install the tools, and finally with the `solvers` option to install the solvers. For instance (parallel installation):
> ```
> make library -s -j
> make tools -s -j
> make solvers -s -j
> ```


### Step 4

An alias (shortcut) to source the global configurations file can be created in the `~/.bashrc` file (or the `~/.profile` file):
```
echo "alias ${FVL_NAME}='source ${FVL_PREFIX}/etc/bashrc.sh'" >> ~/.bashrc
```

The alias will be available after sourcing the edited file or after opening a new Bash shell. Sourcing the global configurations file is required for development and for running the applications (including pre- and post-processing). With the alias created, source the global configurations file automatically whenever needed with the command:
```
fvlib-release
```

The created alias has the same name as the FVLib code directory. In case you have changed its name to `fvlib-release-dbg` for installing the FVLib code in debugging mode, the alias name is `fvlib-release-dbg`.

## 7. Third-party software

The FVLib code has minimal dependencies on third-party software. At the current version of the release, only [Gmsh](https://gmsh.info/) is required to be installed. Version 4.8.4 or above is recommended for compatibility.

Make sure Gmsh is available on your computer through a Bash shell:
```
gmsh --version
```

> **NOTE**: In Ubuntu and other APT-based Linux distributions, Gmsh can be easily installed with `sudo apt-get install gmsh`. For other distributions or to install from source, follow the instructions provided [here](https://gmsh.info/#Download).

